var gulp = require('gulp');
var concat = require('gulp-concat');
var lazyMinify = require('gulp-lazy-minify');
var sass = require('gulp-ruby-sass');
var watch = require('gulp-watch');


gulp.task('sass', function(){
  sass([
    'assets/sass/style.scss',
    'assets/sass/media-query.scss',
  ])
  .pipe(concat('all.css'))
  .pipe(gulp.dest('assets/sass/'))
});

gulp.task('css', function(){
  return gulp.src([
    'assets/bower_components/boostrap/dist/css/bootstrap.min.css',
    'assets/sass/all.css',
  ])
  .pipe(concat('app.css'))
  .pipe(lazyMinify())
  .pipe(gulp.dest('min'))
});

gulp.task('header-js', function(){
  return gulp.src([
    'assets/bower_components/jquery/dist/js/jquery.js',
    'assets/bower_components/boostrap/dist/js/bootstrap.min.js',
  ])
  .pipe(concat('header.js')) 
  .pipe(lazyMinify())
  .pipe(gulp.dest('min'))
});

gulp.task('footer-js', function(){
  return gulp.src([
    'assets/js/custom.js',
  ])
  .pipe(concat('footer.js')) 
  .pipe(lazyMinify())
  .pipe(gulp.dest('min'))
});

gulp.task('watch', function () {
    return  gulp.watch(
    [
      'assets/sass/style.scss',
      'assets/sass/media-query.scss',
      'assets/js/custom.js',
    ], 

    ['sass', 'css', 'header-js', 'footer-js']);
});



gulp.task('default', ['sass', 'css', 'header-js', 'footer-js', 'watch']);
          